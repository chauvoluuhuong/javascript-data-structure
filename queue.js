class Queue {
    constructor(...items) {
        this.items = [...items]
        this.name = 'QueueClass'
    }

    push(...items) {
        this.items.push(...items)
        console.log(`this.items: ${this.items}`)
    }

    printItsName() {
        console.log(`this.name: ${this.name}`)
    }

    get() { // get the first item out
        if (this.items.length > 0) {
            return this.items.shift()
        } else { 
            return null
        }
    }
}

exports.Queue = Queue

const queue = new Queue(1,2,3)

// queue.push(4,5)
// console.log('get first element in quque out: ', queue.get())


function canClassQueueRemainItsThis(cb) {
    cb()
}

/*
    printItsName will not called from queue object  ----> this printItsName will point to its calling Object 
    instead it is called from global object so we have to use bind to bind this to object that we want
*/
canClassQueueRemainItsThis(queue.printItsName.bind(queue)) 