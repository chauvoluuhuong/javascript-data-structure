/*
    Given a number N, the task is to reduce it to a single-digit number by repeatedly 
    subtracting the adjacent digits. That is, in the first iteration, subtract all of the adjacent digits to generate a new number, if this number contains more than one digit, 
    repeat the same process until it becomes a single-digit number.

    Input: N = 6972
        Output: 2
        | 6 – 9 | = 3
        | 9 – 7 | = 2
        | 7 – 2 | = 5
*/

const assert = require('assert')

function solution(n) {
    let singleDigit = null
    if (n > 10) {
        // converting n to array of digits 
        let arr = []
        while(true) {
            arr.unshift(n % 10)
            n = Math.floor( n / 10)
            
            if (n < 10) {
                arr.unshift(n)
                break
            }
        }
        console.log('arr: ', arr)
        
        // caculating new digit by substracting adjacent elements, let's call that new digit is newDigit
        let newDigit = 0
        let digitPosition = 0
        for (let i = arr.length - 1; i > 0; i--) {
            if (digitPosition) newDigit += (10**digitPosition)*Math.abs(arr[i]-arr[i-1]) 
            else newDigit += Math.abs(arr[i]-arr[i-1])
            digitPosition++
        }
    
        // recursively call this so solution with new a argument is newDigit
        return  solution(newDigit)
    } else {
        return n
    }
} 

assert.ok(solution(6972) == 2, `caculating was fail, solution(6972) returned: ${solution(6972)}`)
assert.ok(solution(123456) == 0)
console.log('congratulation !!!')
// console.log(`solution(6972) was: ${solution(6972)}`)