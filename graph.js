class Graph {
    constructor(rootNode) {
        this.rootNode = rootNode

        /*
            * let image that you work with enomous data and you traversal for searching
            * and you'd like to stop searching whenever your data being searched
        */
        this.shouldStopTraversal = false  
    }

    addRoot(rootNode) {
        this.rootNode = rootNode
    }

    /**
     * root => left => right
     * @param {any} cb the callback function
     * @param {any} node the node to begin traversal
     * @returns {any}
     */
    traversal(cb, node, nodesTraversaled) {
        if (node && !this.shouldStopTraversal) {
            console.log('traversaled node: ', node.data)
            console.log('nodes in node: ', node.nodes)
            console.log('\n\n')
            
            for (let _node of node.nodes) {
                this.traversal(cb, _node)
            }
        } 
    }
}

class Node {
    constructor(data, leftNode, rightNode) {
        this.data = data
        this.nodes = []
    } 
}

var graph = new Graph()

var rootNode = new Node('this is rootNode')
var node1 = new Node('this is node 1')
var node2 = new Node('this is node 2')
var node3 = new Node('this is node 3')
var node4 = new Node('this is node 4')
var node5 = new Node('this is node 5')

binaryTree.addRoot(rootNode)
rootNode.nodes.push(node1, node2, node3)
node1.nodes.push(node4, node5)

binaryTree.traversal(null, binaryTree.rootNode)
