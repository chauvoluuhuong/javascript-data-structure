class Tree {
    constructor(rootNode) {
        this.rootNode = rootNode

        /*
            * let image that you work with enomous data and you traversal for searching
            * and you'd like to stop searching whenever your data being searched
        */
        this.shouldStopTraversal = false  
    }

    addRoot(rootNode) {
        this.rootNode = rootNode
    }

    /**
     * root => left => right
     * @param {any} cb the callback function
     * @param {any} node the node to begin traversal
     * @returns {any}
     */
    traversal(cb, rootNode) {
        if (rootNode && !this.shouldStopTraversal) {
            console.log('traversaled rootNode: ', rootNode.data)
            console.log('nodes in rootNode: ', rootNode.nodes)
            console.log('\n\n')

            for (let node of rootNode.nodes) {
                this.traversal(cb, node)
            }
        } 
    }
}

class Node {
    constructor(data, leftNode, rightNode) {
        this.data = data
        this.nodes = []
    } 
}

var binaryTree = new Tree()

var rootNode = new Node('this is rootNode')
var node1 = new Node('this is node 1')
var node2 = new Node('this is node 2')
var node3 = new Node('this is node 3')
var node4 = new Node('this is node 4')
var node5 = new Node('this is node 5')

tree.addRoot(rootNode)
rootNode.nodes.push(node1, node2, node3)
node1.nodes.push(node4, node5)

tree.traversal(null, binaryTree.rootNode)
