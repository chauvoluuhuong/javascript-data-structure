class LinkedList {
    /**
     * brief: 
     * @param {Node} node
     * @returns {LinkedList}
     */
    constructor(node) {
        this.head = node
    }

    addNode(node) {
        if (!this.head) {
            this.head = node
        } else {
            this._findTailNode().nextNode = node 
        }
    }

    _findTailNode() {
        if (!this.head) return this.head

        let currentNode = this.head
        while(currentNode.nextNode) {
            currentNode = currentNode.nextNode
            console.log('traversaled node: ', currentNode.data)
        }

        return currentNode
    }

    findNode(node) {
        if (!this.head) return null

        let currentNode = this.head
        while(currentNode.nextNode) {
            if (currentNode == node) return currentNode
            currentNode = currentNode.nextNode
        }

        return currentNode
    }
}

class Node {
    constructor(data, nextNode) {
        this.data = data // assume that the node used to store some of data
        this.nextNode = nextNode
    }
}

var linkedList = new LinkedList()

linkedList.addNode(new Node('this is node One'))
linkedList.addNode(new Node('this is node two'))
linkedList.addNode(new Node('this is node tree'))
linkedList.addNode(new Node('this is node four'))
linkedList.addNode(new Node('this is node five'))

console.log('tail node of list: ', linkedList._findTailNode())