class BinaryTree {
    constructor(rootNode) {
        this.rootNode = rootNode

        /*
            * let image that you work with enomous data and you traversal for searching
            * and you'd like to stop searching whenever your data being searched
        */
        this.shouldStopTraversal = false  
    }

    addRoot(rootNode) {
        this.rootNode = rootNode
    }

    /**
     * root => left => right
     * @param {any} cb the callback function
     * @param {any} node the node to begin traversal
     * @returns {any}
     */
    traversalPreoder(cb, node) {
        let currentNode = this.rootNode
        let nodesNeedToTraversal = [currentNode]
        
        while(nodesNeedToTraversal.length > 0) {
            let newNodes = []
            for (let i in nodesNeedToTraversal) {
                    currentNode = nodesNeedToTraversal.shift()
                    console.log(`traversal to 
                                rootNode: ${currentNode.data}
                                currentNode.leftNode.data: ${currentNode.leftNode && currentNode.leftNode.data} 
                                currentNode.rightNode.data: ${currentNode.rightNode && currentNode.rightNode.data}\n`)
                    currentNode.leftNode && newNodes.push(currentNode.leftNode) 
                    //yup traversaled leftNode
                    //do some logic here
                    currentNode.rightNode && newNodes.push(currentNode.rightNode)
                    //yup traversaled rightNode
                    //do some logic here
            }

            nodesNeedToTraversal = nodesNeedToTraversal.concat(newNodes)
        }
    }

    //left => root => right
    traversalInOder(cb) {

    }

    //left => right => root
    traversalPostOder(cb) {

    }
}

class Node {
    constructor(data, leftNode, rightNode) {
        this.data = data
        this.leftNode = leftNode
        this.rightNode = rightNode
    } 
}

var binaryTree = new BinaryTree()

var rootNode = new Node('this is rootNode')
var node1 = new Node('this is node 1')
var node2 = new Node('this is node 2')
var node3 = new Node('this is node 3')
var node4 = new Node('this is node 4')
var node5 = new Node('this is node 5')

binaryTree.addRoot(rootNode)
rootNode.leftNode = node1
rootNode.rightNode = node2

node1.leftNode = node3
node1.rightNode = node4

node2.leftNode = node5

binaryTree.traversalPreoder(null, binaryTree.rootNode)
