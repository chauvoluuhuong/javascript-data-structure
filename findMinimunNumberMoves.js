/*
    Given an array arr[] of size N where every element is from the range [0, 9]. The task is to reach the last index of the array starting from the first index. 
    From ith index we can move to (i – 1)th, (i + 1)th or to any jth index where j ≠ i and arr[j] = arr[i].
*/

/*
    describe my solution at high level.
    let assume that  element's index as a nodes. 
    For exame if we have array arr[] = {1, 2, 3, 4, 1, 5} we can have four nodes 0, 1, 2, 3, 4, 5
    each node store two properties as describe below: 
        {
            nodeName: indexOfThisNodes,
            reachAbleNodes: [<element's indexs that have the same value and indexOfThisNodes + 1,  indexOfThisNodes - 1>] 
        }
        
    let visualize nodes linkes together and become a graph. 
    you can interpret this question to: 
    given a graph with nodes. each node linked to another node by values in  reachAbleNodes. for example if you have node 
    {
        nodeName: 1,
        reachAbleNodes: [2,3]
    }

    that mean nodeName 1 will be linked to node 2 and 3.
    from the starting point is node 1, let find all ways that can traverse to end node. the nodeName of end node will be last index of given array 
*/

class Graph {
    constructor() {
        this.endPoint = null
        this.nodes = []
        this.travesalPaths = []
    }

    addNode(nodeName, reachAbleNodes) {
        this.nodes.push({
            nodeName: nodeName,
            reachAbleNodes: reachAbleNodes
        })
    }

    findReachAbleNodes(nodeName, givenArray) {
        var reachAbleNodes = []
        if((nodeName + 1) < givenArray.length) reachAbleNodes.push(nodeName+1)
        if((nodeName - 1) >= 0) reachAbleNodes.push(nodeName - 1)
        for (let i=0; i < givenArray.length; i++) {
            (givenArray[i] == givenArray[nodeName]) && (nodeName != i) && reachAbleNodes.push(i)
        }
        return reachAbleNodes
    }

    findAllPathsFromStartingPointToEndPoint() {
        for (let reachAble of this.nodes[0].reachAbleNodes) { // first paths 
            this.travesalPaths.push([this.nodes[0], reachAble])
        }
        let nodeTraversed = [this.nodes[0], ...this.nodes[0].reachAbleNodes]
        
        let shouldLookingForNewNode = true
        while(shouldLookingForNewNode) {
            shouldLookingForNewNode = false

            for (let traversalPathIndex of this.travesalPaths) {
                let lastNodeInTraversalPath = this.travesalPaths[traversalPathIndex][this.travesalPaths.length - 1]     

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////append new node to existing path
                if(nodeTraversed.indexOf(lastNodeInTraversalPath.reachAbleNodes[0]) == -1) {
                    this.travesalPaths[traversalPathIndex].push(lastNodeInTraversalPath.reachAbleNodes[0])
                    nodeTraversed.push(lastNodeInTraversalPath.reachAbleNodes[0])
                    shouldLookingForNewNode = true
                } 
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////create new from existing paths and new readable nodes 
                for (let i = 1; i < lastNodeInTraversalPath.reachAbleNodes; i++) {
                    if(nodeTraversed.indexOf(lastNodeInTraversalPath.reachAbleNodes[i]) == -1) {
                        this.travesalPaths.push([...this.travesalPaths[traversalPathIndex], lastNodeInTraversalPath.reachAbleNodes[i]])
                        nodeTraversed.push(lastNodeInTraversalPath.reachAbleNodes[i])    
                        shouldLookingForNewNode = true
                    } 
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        }
        
        return this.travesalPaths
    }
}

graph = new Graph()
let testArr = [1, 2, 3, 4, 1, 5]

let reachAblenodes = testArr.map((e, id) => graph.findReachAbleNodes(id, testArr))

for (let i in reachAblenodes) {
    graph.addNode(i, reachAblenodes[i])
}

// graph.nodes[0] = nodes[0] // add starting node

console.log('travesalPaths: ', graph.findAllPathsFromStartingPointToEndPoint())