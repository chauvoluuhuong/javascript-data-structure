'''
https://www.geeksforgeeks.org/optimal-strategy-for-a-game-set-3/
Consider a row of n coins of values v1 . . . vn, where n is even. 
We play a game against an opponent by alternating turns. In each turn, 
a player selects either the first or last coin from the row, removes it from the row permanently, and receives the value of the coin. 
Determine the maximum possible amount of money we can definitely win if we move first.
Also, print the sequence of moves in the optimal game. As many sequences of moves may lead to the optimal answer, you may print any valid sequence.

solution:
    frist of all, obviously, the strategy to win this game if i move first is very simple.
    i just alway choose the big one in array of coin at my turn.
    of course with given choosing condition

'''

'''
    @para: coins: array of coins
'''
def solution(coins):
    """
        map posible choosing coins at each turn
    """
    lengthOfCoins = len(coins)
    possibleChoosingPairs = [[coins[i], coins[lengthOfCoins-1-i]] for i in range(int(6/2))]
    print('posibleChoosingPairs: {}'.format(possibleChoosingPairs))
    
    '''
        caculate the maximum posible amount of money
    '''
    maximumValueCoinsChoosedFromFristUser = [   possibleChoosingPair[0] 
                                                if possibleChoosingPair[0] > possibleChoosingPair[1] else possibleChoosingPair[1]
                                                for possibleChoosingPair in  possibleChoosingPairs    
                                            ]
    print('maximumValueCoinsChoosedFromFristUser: {}'.format(maximumValueCoinsChoosedFromFristUser))
    maxPosibleAmountOfMoney = sum(maximumValueCoinsChoosedFromFristUser)
    return maxPosibleAmountOfMoney


solution([1,2,5,10,20,100])